﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fountain : MonoBehaviour
{

    [SerializeField] private GameObject FluidParticle;

    [SerializeField] private float force;
    [SerializeField] private float angle;
    [SerializeField] private float rate;

    [SerializeField] private int MaxPartice;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Fountainfun(rate, force, MaxPartice, angle));
    }

    // Update is called once per frame
    IEnumerator Fountainfun(float rate, float force, int MaxPartice,float angle)
    {
        int counter = 0;
        GameObject tmpObj;
        Vector2 boom = new Vector2(1,(1+angle)*force);
        while (counter<=MaxPartice)
        {
            tmpObj=Instantiate(FluidParticle, gameObject.transform);
            tmpObj.GetComponent<Rigidbody2D>().AddForce(boom);
            counter++;
            yield return new WaitForSeconds(rate);
        }
    }
}
