﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraZoom : MonoBehaviour
{
    [SerializeField] private Camera _camera;
    private float startscaling;
    [SerializeField] private float zoom;

    private void Awake()
    {
        startscaling = _camera.orthographicSize;
    }


    public void ZoomOut()
    {
        _camera.orthographicSize = zoom;
    }
}
