﻿using UnityEngine;
using UnityEngine.InputSystem;

public class InputController : MonoBehaviour
{
    private bool usingKeyboard;

    private Vector2 moveDirection;
    public float moveSpeed = 1f;

    private bool jumping;
    private bool isOnGround;
    [SerializeField] private float timeOnGround = 0.1f;
    private float groundTimer;
    private bool walking;
    [SerializeField] private LayerMask whatIsGround;
    [SerializeField] float checkGroundDistance = 0.5f;
    [SerializeField] float checkGroundSize = 0.5f;
    public float jumpForce = 10f;
    private bool isInJump;
    [SerializeField] public float afterJumpGravity = 1.5f;

    private bool throwing;
    private bool isPlayerOrb;
    private bool canThrow = true;
    private Vector2 throwDirection;
    [SerializeField] private GameObject environmentOrbPrefab;
    private GameObject environmentOrb;
    [SerializeField] private GameObject playerOrbPrefab;
    private GameObject playerOrb;
    [SerializeField] private Vector3 offset;
    [SerializeField] private float timeUntilIdle = 0.1f;
    private float idleTimer;

    [SerializeField] private GameObject indicator;
    private Animator animator;
    private AudioSource audioSource;
    private Rigidbody2D rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
        idleTimer = timeUntilIdle;
        groundTimer = timeOnGround;
    }

    public void HandleMove(InputAction.CallbackContext context)
    {
        moveDirection = context.ReadValue<Vector2>();
        if (moveDirection.x == 0f)
        {
            walking = false;
        }
        else
        {
            walking = true;
        }
    }
    //TODO: Kommt 2 Mal in die Methode mit einem Klick
    public void HandleJump(InputAction.CallbackContext context)
    {
        if (isOnGround && groundTimer < 0f)
        {
            jumping = true;
            groundTimer = timeOnGround;
        }
    }
    //TODO: Kommt 2 Mal in die Methode mit einem Klick
    public void HandleThrowEnvironmentOrb(InputAction.CallbackContext context)
    {
        if (canThrow && groundTimer < 0f && !walking && idleTimer < 0f && rb.velocity.y < 0.1f && rb.velocity.y > -0.1f)
        {
            canThrow = false;
            throwing = true;
            isPlayerOrb = false;
        }
    }

    public void HandleThrowPlayerOrb(InputAction.CallbackContext context)
    {
        if (canThrow && groundTimer < 0f && !walking && idleTimer < 0f && rb.velocity.y < 0.1f && rb.velocity.y > -0.1f)
        {
            canThrow = false;
            throwing = true;
            isPlayerOrb = true;
        }
    }

    public void HandleThrowDirectionController(InputAction.CallbackContext context)
    {
        throwDirection = context.ReadValue<Vector2>();
        RotateIndicator();
    }

    public void HandleThrowDirectionKeyboardMouse(InputAction.CallbackContext context)
    {
        if (Camera.main != null)
        {
            throwDirection = (Camera.main.ScreenToWorldPoint(context.ReadValue<Vector2>()) - (transform.position + offset)).normalized;
        }
        RotateIndicator();
    }

    public void HandleOrbDestroy(InputAction.CallbackContext context)
    {
        if(environmentOrb)
        {
            environmentOrb.GetComponent<Orb>().DestroyOrb();
        }
        
        if(playerOrb)
        {
            playerOrb.GetComponent<Orb>().DestroyOrb();
        }
    }

    private void FixedUpdate()
    {
        isOnGround = (Physics2D.Linecast(new Vector2(transform.position.x - checkGroundSize, transform.position.y - checkGroundDistance), new Vector2(transform.position.x - checkGroundSize, transform.position.y - checkGroundDistance - 0.05f), whatIsGround) || Physics2D.Linecast(new Vector2(transform.position.x + checkGroundSize, transform.position.y - checkGroundDistance), new Vector2(transform.position.x + checkGroundSize, transform.position.y - checkGroundDistance - 0.05f), whatIsGround));
        RotateIndicator();

        if (throwing)
        {
            animator.SetBool("throwing", true);
            indicator.SetActive(true);
        }
        else
        { 
            if (isOnGround)
            {
                isInJump = false;
                animator.SetBool("jumping", false);
                rb.gravityScale = 1f;
                groundTimer -= Time.fixedDeltaTime;
            }
            else
            {
                groundTimer = timeOnGround;
                idleTimer = timeUntilIdle;
                isInJump = true;
                animator.SetBool("jumping", true);
                indicator.SetActive(false);
            }

            if (jumping)
            {
                jumping = false;
                isInJump = true;
                animator.SetBool("jumping", true);
                rb.velocity = new Vector2(rb.velocity.x, jumpForce);
                idleTimer = timeUntilIdle;
                indicator.SetActive(false);
            }

            if (isInJump)
            {
                if (rb.velocity.y < 0)
                {
                    rb.gravityScale = afterJumpGravity;
                }
                idleTimer = timeUntilIdle;
                indicator.SetActive(false);
            }

            rb.velocity = new Vector2(moveSpeed * moveDirection.x, rb.velocity.y);
            animator.SetFloat("yVelocity", rb.velocity.y);

            if (rb.velocity.x < -0.1f)
            {
                transform.rotation = Quaternion.Euler(transform.rotation.x, 0f, transform.rotation.z);
                animator.SetFloat("xVelocity", -rb.velocity.x);
                idleTimer = timeUntilIdle;
                indicator.SetActive(false);
            }
            else if (rb.velocity.x > 0.1f)
            {
                transform.rotation = Quaternion.Euler(transform.rotation.x, 180f, transform.rotation.z);
                animator.SetFloat("xVelocity", rb.velocity.x);
                idleTimer = timeUntilIdle;
                indicator.SetActive(false);
            }
            else
            {
                if (idleTimer < 0f)
                {
                    animator.SetFloat("xVelocity", 0f);
                    indicator.SetActive(true);
                }
                else
                {
                    idleTimer -= Time.fixedDeltaTime;
                }
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.white;
        Gizmos.DrawLine(new Vector2(transform.position.x - checkGroundSize, transform.position.y - checkGroundDistance), new Vector2(transform.position.x - checkGroundSize, transform.position.y - checkGroundDistance - 0.05f));
        Gizmos.DrawLine(new Vector2(transform.position.x + checkGroundSize, transform.position.y - checkGroundDistance), new Vector2(transform.position.x + checkGroundSize, transform.position.y - checkGroundDistance - 0.05f));
        //Gizmos.DrawWireSphere(transform.position + offset, 0.5f);
    }

    public void SetCanThrow(bool b)
    {
        canThrow = b;
    }

    public void SpawnOrb()
    {
        float rotZ = Mathf.Atan2(-throwDirection.normalized.x, throwDirection.normalized.y) * Mathf.Rad2Deg;
        if (isPlayerOrb)
        {
            playerOrb = Instantiate(playerOrbPrefab, transform.position + offset, Quaternion.Euler(0f, 0f, rotZ));
        }
        else
        {
            environmentOrb = Instantiate(environmentOrbPrefab, transform.position + offset, Quaternion.Euler(0f, 0f, rotZ));
        }
        animator.SetBool("throwing", false);
        throwing = false;
    }

    public void RotateIndicator()
    {
        float rotZ = Mathf.Atan2(-throwDirection.normalized.x, throwDirection.normalized.y) * Mathf.Rad2Deg;
        indicator.transform.rotation = Quaternion.Euler(0f, 0f, rotZ);
    }

    public void PlaySound(AudioClip clip)
    {
        if (clip)
        {
            audioSource.clip = clip;
            audioSource.Play();
        }
        else
        {
            audioSource.clip = null;
        }
    }
}
