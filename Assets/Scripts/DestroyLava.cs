﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;    

public class DestroyLava : MonoBehaviour
{
    [SerializeField] private int _lavaLayer;
    

    private void OnCollisionEnter2D(Collision2D other)
    {
        
        if (_lavaLayer.Equals(other.gameObject.layer))
        {
            Destroy(gameObject);
        }
    }
}
