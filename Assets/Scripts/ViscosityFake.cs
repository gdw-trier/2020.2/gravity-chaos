﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViscosityFake : MonoBehaviour
{
    //viscosity var
    [SerializeField] private float maxVelocity = 3f;
    private Rigidbody2D rb;
    
    
    //2d wiggle var
    private bool left;
    private Vector2 wiggle;
    [Range(0.0f, 10000.0f)] [SerializeField] private float wiggleforce = 0;
    
    //single Time to live var
    [SerializeField] private float ttl = 4;
    private float counter;
    private List<Collider2D> nearParticles = new List<Collider2D>();
    private ContactFilter2D _filter2D;
    [SerializeField] private float radius = 1f;
    [SerializeField] private LayerMask _lavaparticle;
    
    
    
    
    private void Start()
    {
      rb = gameObject.GetComponent<Rigidbody2D>();
     // wiggle = new Vector2(wiggleforce*Random.Range(0f,1f),(wiggleforce*Random.Range(0f,1f)));
     wiggle = wiggleforce*Random.insideUnitCircle.normalized*Random.Range(0.5f,1f);
      counter = ttl;
      _filter2D.layerMask = _lavaparticle;
      _filter2D.useLayerMask = true;
    }

    private void FixedUpdate()
    {
        
        
        //viscosity
       if(rb.velocity.magnitude > maxVelocity)
         {
            rb.velocity = rb.velocity.normalized * maxVelocity;
         }  
        
        if (left)
        {
            wiggle = -wiggle;
            left = !left;
        }
        else
        {
            wiggle.x = -wiggle.x;
            left = !left; 
        }
        rb.AddForce(wiggle);
        //Time to Live
        if(1 < Physics2D.OverlapCircle(new Vector2(transform.position.x, transform.position.y), radius, _filter2D,nearParticles))
        {
            counter = ttl;
            nearParticles.Clear();
        }
        else
        {
            if(counter<0)
                Destroy(gameObject);
            counter -= Time.fixedDeltaTime;
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag.Equals("Player"))
            other.gameObject.GetComponent<PlayerDeath>().Death();
    }


    /*[SerializeField] private float Klebkraft = 0.1f;
    [SerializeField] private float Kleberadius = 1f;
    [SerializeField] private LayerMask _lavaparticle;
    private List<Collider2D> nearParticles = new List<Collider2D>();
    private ContactFilter2D _filter2D;
    private Rigidbody2D _rb;
    private bool physics_bool = true;
    
    

    private void Start()
    {
        _filter2D.layerMask = _lavaparticle;
        _filter2D.useLayerMask = true;
        _rb = gameObject.GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        if (physics_bool)
        {
            Physics2D.OverlapCircle(new Vector2(transform.position.x, transform.position.y), Kleberadius, _filter2D,
                nearParticles);
            foreach (Collider2D x in nearParticles)
            {
                _rb.AddForce(x.transform.position * Klebkraft);
                Debug.Log(x.gameObject.name);
            }
            nearParticles.Clear();
           // physics_bool = false;
           // StartCoroutine(timer(physics_bool));
        }


    }

    IEnumerator timer(bool physics_bool)
    {
        yield return new WaitForSeconds(0.3f);
        physics_bool = true;
    }
    */
    void OnDrawGizmosSelected()
    {
        // Draw a yellow sphere at the transform's position
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, radius);
        Gizmos.DrawLine(rb.position,new Vector3(rb.position.x+wiggle.x,rb.position.y+wiggle.y,0));
        
        }
}
