﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BagOrbs : MonoBehaviour
{
    private Rigidbody2D rb;

    [SerializeField] private float maxVelocity = 3f;
    [SerializeField] private float maxDistance = 1.5f;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        if(Vector2.Distance(transform.parent.position, transform.position) > maxDistance)
        {
            transform.position = transform.parent.position;
        }

        if(rb.velocity.magnitude > maxVelocity)
        {
            rb.velocity = rb.velocity.normalized * maxVelocity;
        }
    }

}
