﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbGravity : MonoBehaviour
{
    [SerializeField] private float orbGravity = 1f;
    [SerializeField] private float gravityRange = 10f;
    private List<Collider2D> affectedObjects;
    [SerializeField] private LayerMask whatIsAffected;
    private ContactFilter2D filter;
    private Rigidbody2D affectedRb;

    [SerializeField] private float timeUntilActivated = 0.5f;

    private void Awake()
    {
        affectedObjects = new List<Collider2D>();
        filter.layerMask = whatIsAffected;
        filter.useLayerMask = true;
    }

    private void FixedUpdate()
    {
        if (timeUntilActivated < 0f)
        {
            Physics2D.OverlapCircle(transform.position, gravityRange, filter, affectedObjects);
            foreach (Collider2D affectedObj in affectedObjects)
            {
                affectedRb = affectedObj.GetComponent<Rigidbody2D>();
                if (affectedRb)
                {
                    Vector2 dir = transform.position - affectedObj.transform.position;
                    float distanceFactor = (1 - ((Vector2.Distance(affectedObj.transform.position, transform.position)) / gravityRange));
                    if (distanceFactor > 0f)
                    {
                        affectedRb.AddForce(dir * orbGravity * distanceFactor);
                        affectedRb.gravityScale = 0f;
                    }
                    else
                    {
                        if (affectedObj.gameObject.CompareTag("Player"))
                        {
                            affectedRb.gravityScale = affectedObj.GetComponent<InputController>().afterJumpGravity;
                        }
                        else
                        {
                            affectedRb.gravityScale = 1f;
                        }
                    }
                }
            }
        }
        else
        {
            timeUntilActivated -= Time.fixedDeltaTime;
        }
    }

    private void OnDestroy()
    {
        Physics2D.OverlapCircle(transform.position, gravityRange, filter, affectedObjects);
        foreach (Collider2D affectedObj in affectedObjects)
        {
            affectedRb = affectedObj.GetComponent<Rigidbody2D>();

            if (affectedRb)
            {
                if (affectedObj.gameObject.CompareTag("Player"))
                {
                    affectedRb.gravityScale = affectedObj.GetComponent<InputController>().afterJumpGravity;
                }
                else
                {
                    affectedRb.gravityScale = 1f;
                }
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.black;
        Gizmos.DrawWireSphere(transform.position, gravityRange);
    }
}
