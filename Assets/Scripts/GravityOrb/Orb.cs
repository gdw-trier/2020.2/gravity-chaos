﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orb : MonoBehaviour
{
    [SerializeField] private float moveSpeed = 5f;
    [SerializeField] private float moveTime = 2f;
    private float timeMoved;
    [SerializeField] private float stayTime = 3f;
    private GameObject player;
    private bool done;

    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");   
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (timeMoved < moveTime)
        {
            timeMoved += Time.fixedDeltaTime;
            transform.Translate(Vector3.up * moveSpeed * (1 - (timeMoved / moveTime)) * Time.fixedDeltaTime);
        }
        else if(timeMoved > moveTime && stayTime > 0f)
        {
            stayTime -= Time.fixedDeltaTime;
        }
        else if(timeMoved > moveTime && stayTime < 0f && !done)
        {
            DestroyOrb();
        }
    }

    public void DestroyOrb()
    {
        done = true;
        player.GetComponent<InputController>().SetCanThrow(true);
        //insert fancy effects here
        Destroy(gameObject);
    }
}
