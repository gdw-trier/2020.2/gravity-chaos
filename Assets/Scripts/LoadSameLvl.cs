﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class LoadSameLvl
{
   public static void load(int buildindex)
   {
       SceneManager.LoadSceneAsync(buildindex,LoadSceneMode.Single);
   }
}
