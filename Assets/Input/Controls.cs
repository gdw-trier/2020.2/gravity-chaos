// GENERATED AUTOMATICALLY FROM 'Assets/Input/Controls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @Controls : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @Controls()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""Controls"",
    ""maps"": [
        {
            ""name"": ""PlayerMovement"",
            ""id"": ""5503296d-61cc-47d5-9c29-9886f79d666c"",
            ""actions"": [
                {
                    ""name"": ""Walk"",
                    ""type"": ""Value"",
                    ""id"": ""1975b073-6fbe-49f3-bf70-174e08f9a711"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Jump"",
                    ""type"": ""Button"",
                    ""id"": ""813310ba-22f8-484e-9539-429df6b712eb"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ThrowEnvironmentOrb"",
                    ""type"": ""Button"",
                    ""id"": ""3d0d60df-c1f6-4906-928b-3472cf0494b1"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ThrowDirectionController"",
                    ""type"": ""PassThrough"",
                    ""id"": ""ab18e743-a63a-489b-8017-6208ea33262d"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""DestroyOrb"",
                    ""type"": ""Button"",
                    ""id"": ""e2724d2b-915a-46b1-98cf-f44f051d6f73"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ThrowPlayerOrb"",
                    ""type"": ""Button"",
                    ""id"": ""3c6e636e-15fb-4c0e-a0cb-9e5111d0dd40"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Reset"",
                    ""type"": ""Button"",
                    ""id"": ""424b1b32-08b3-43c5-a5cd-39418e324229"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ThrowDirectionKeyboardMouse"",
                    ""type"": ""PassThrough"",
                    ""id"": ""f4ae60a8-5bf5-4373-b415-5e7f329e9349"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""Controller DPad"",
                    ""id"": ""6afda3cb-7ef2-478c-b667-04770267247f"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Walk"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""6f5508c1-cca8-4273-ad72-6758d0692cd9"",
                    ""path"": """",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""Walk"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""b414f46e-7ad7-4aa2-8f48-c2530db9fde4"",
                    ""path"": """",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""Walk"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""0eff19b6-6b72-4c3a-99a7-798b18937e86"",
                    ""path"": ""<Gamepad>/dpad/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""Walk"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""76c6fdb5-c69b-4eb2-994e-f9288c4b51e8"",
                    ""path"": ""<Gamepad>/dpad/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""Walk"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Controller Stick"",
                    ""id"": ""e79af44f-3821-41e2-8472-99ca611df3f9"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Walk"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""de34ce98-f993-4b44-a4ef-376ec3aaa797"",
                    ""path"": """",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""Walk"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""2ef03a4d-58f5-4114-8ab9-ce903696d4d6"",
                    ""path"": """",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""Walk"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""fff792db-33b3-4fc4-867b-eca1e95640af"",
                    ""path"": ""<Gamepad>/leftStick/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""Walk"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""171e3bfc-2301-4502-9fc2-07d3ccb75b47"",
                    ""path"": ""<Gamepad>/leftStick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""Walk"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""2D Vector"",
                    ""id"": ""f778b7ac-2b69-4549-ac08-9ab78814c2a1"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Walk"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""006073a4-b279-4759-8a4b-01dee27b7f27"",
                    ""path"": """",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardMouse"",
                    ""action"": ""Walk"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""b21e0334-05be-42e6-9c07-473a0b720bac"",
                    ""path"": """",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardMouse"",
                    ""action"": ""Walk"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""0a74bc33-781e-478d-9b17-bdfeb6946317"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardMouse"",
                    ""action"": ""Walk"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""703e8587-061b-48f8-b4ea-0ddfd6f42aa2"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardMouse"",
                    ""action"": ""Walk"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""9d869033-eea8-4241-849e-c4ab09d021d2"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""5da2a157-b51d-4a8f-b506-9d56b87c47f4"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardMouse"",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9e1351b3-8aa9-4657-ae71-222a29225b71"",
                    ""path"": ""<Gamepad>/rightTrigger"",
                    ""interactions"": ""Tap"",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""ThrowEnvironmentOrb"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1c6f8523-23e0-4b3e-bb36-f761d8dfed31"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardMouse"",
                    ""action"": ""ThrowEnvironmentOrb"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d9be277d-57c9-44a6-91fb-3dd470bcf60f"",
                    ""path"": ""<Gamepad>/rightStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""ThrowDirectionController"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""075fce7c-5c58-47a3-8023-79637f80d2ef"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""DestroyOrb"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""370db92c-e3a7-4516-9ae3-4ad7798c29d4"",
                    ""path"": ""<Mouse>/middleButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardMouse"",
                    ""action"": ""DestroyOrb"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b87889a0-2a9a-49ea-8787-34a20799b30b"",
                    ""path"": ""<Gamepad>/leftTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""ThrowPlayerOrb"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b10f1651-f624-460d-8448-ad46390a9037"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardMouse"",
                    ""action"": ""ThrowPlayerOrb"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""5f70e34c-1f08-46f1-abbe-c984d61f7c14"",
                    ""path"": ""<Gamepad>/buttonNorth"",
                    ""interactions"": ""Press(behavior=1)"",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""Reset"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""610b4cc1-5b17-4239-ad08-e3ad09d241e9"",
                    ""path"": ""<Keyboard>/r"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardMouse"",
                    ""action"": ""Reset"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""86d3b9a8-b008-4a9d-b937-9492bcf04b60"",
                    ""path"": ""<Mouse>/position"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardMouse"",
                    ""action"": ""ThrowDirectionKeyboardMouse"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""Controller"",
            ""bindingGroup"": ""Controller"",
            ""devices"": [
                {
                    ""devicePath"": ""<Gamepad>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        },
        {
            ""name"": ""KeyboardMouse"",
            ""bindingGroup"": ""KeyboardMouse"",
            ""devices"": [
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": false,
                    ""isOR"": false
                },
                {
                    ""devicePath"": ""<Mouse>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
        // PlayerMovement
        m_PlayerMovement = asset.FindActionMap("PlayerMovement", throwIfNotFound: true);
        m_PlayerMovement_Walk = m_PlayerMovement.FindAction("Walk", throwIfNotFound: true);
        m_PlayerMovement_Jump = m_PlayerMovement.FindAction("Jump", throwIfNotFound: true);
        m_PlayerMovement_ThrowEnvironmentOrb = m_PlayerMovement.FindAction("ThrowEnvironmentOrb", throwIfNotFound: true);
        m_PlayerMovement_ThrowDirectionController = m_PlayerMovement.FindAction("ThrowDirectionController", throwIfNotFound: true);
        m_PlayerMovement_DestroyOrb = m_PlayerMovement.FindAction("DestroyOrb", throwIfNotFound: true);
        m_PlayerMovement_ThrowPlayerOrb = m_PlayerMovement.FindAction("ThrowPlayerOrb", throwIfNotFound: true);
        m_PlayerMovement_Reset = m_PlayerMovement.FindAction("Reset", throwIfNotFound: true);
        m_PlayerMovement_ThrowDirectionKeyboardMouse = m_PlayerMovement.FindAction("ThrowDirectionKeyboardMouse", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // PlayerMovement
    private readonly InputActionMap m_PlayerMovement;
    private IPlayerMovementActions m_PlayerMovementActionsCallbackInterface;
    private readonly InputAction m_PlayerMovement_Walk;
    private readonly InputAction m_PlayerMovement_Jump;
    private readonly InputAction m_PlayerMovement_ThrowEnvironmentOrb;
    private readonly InputAction m_PlayerMovement_ThrowDirectionController;
    private readonly InputAction m_PlayerMovement_DestroyOrb;
    private readonly InputAction m_PlayerMovement_ThrowPlayerOrb;
    private readonly InputAction m_PlayerMovement_Reset;
    private readonly InputAction m_PlayerMovement_ThrowDirectionKeyboardMouse;
    public struct PlayerMovementActions
    {
        private @Controls m_Wrapper;
        public PlayerMovementActions(@Controls wrapper) { m_Wrapper = wrapper; }
        public InputAction @Walk => m_Wrapper.m_PlayerMovement_Walk;
        public InputAction @Jump => m_Wrapper.m_PlayerMovement_Jump;
        public InputAction @ThrowEnvironmentOrb => m_Wrapper.m_PlayerMovement_ThrowEnvironmentOrb;
        public InputAction @ThrowDirectionController => m_Wrapper.m_PlayerMovement_ThrowDirectionController;
        public InputAction @DestroyOrb => m_Wrapper.m_PlayerMovement_DestroyOrb;
        public InputAction @ThrowPlayerOrb => m_Wrapper.m_PlayerMovement_ThrowPlayerOrb;
        public InputAction @Reset => m_Wrapper.m_PlayerMovement_Reset;
        public InputAction @ThrowDirectionKeyboardMouse => m_Wrapper.m_PlayerMovement_ThrowDirectionKeyboardMouse;
        public InputActionMap Get() { return m_Wrapper.m_PlayerMovement; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PlayerMovementActions set) { return set.Get(); }
        public void SetCallbacks(IPlayerMovementActions instance)
        {
            if (m_Wrapper.m_PlayerMovementActionsCallbackInterface != null)
            {
                @Walk.started -= m_Wrapper.m_PlayerMovementActionsCallbackInterface.OnWalk;
                @Walk.performed -= m_Wrapper.m_PlayerMovementActionsCallbackInterface.OnWalk;
                @Walk.canceled -= m_Wrapper.m_PlayerMovementActionsCallbackInterface.OnWalk;
                @Jump.started -= m_Wrapper.m_PlayerMovementActionsCallbackInterface.OnJump;
                @Jump.performed -= m_Wrapper.m_PlayerMovementActionsCallbackInterface.OnJump;
                @Jump.canceled -= m_Wrapper.m_PlayerMovementActionsCallbackInterface.OnJump;
                @ThrowEnvironmentOrb.started -= m_Wrapper.m_PlayerMovementActionsCallbackInterface.OnThrowEnvironmentOrb;
                @ThrowEnvironmentOrb.performed -= m_Wrapper.m_PlayerMovementActionsCallbackInterface.OnThrowEnvironmentOrb;
                @ThrowEnvironmentOrb.canceled -= m_Wrapper.m_PlayerMovementActionsCallbackInterface.OnThrowEnvironmentOrb;
                @ThrowDirectionController.started -= m_Wrapper.m_PlayerMovementActionsCallbackInterface.OnThrowDirectionController;
                @ThrowDirectionController.performed -= m_Wrapper.m_PlayerMovementActionsCallbackInterface.OnThrowDirectionController;
                @ThrowDirectionController.canceled -= m_Wrapper.m_PlayerMovementActionsCallbackInterface.OnThrowDirectionController;
                @DestroyOrb.started -= m_Wrapper.m_PlayerMovementActionsCallbackInterface.OnDestroyOrb;
                @DestroyOrb.performed -= m_Wrapper.m_PlayerMovementActionsCallbackInterface.OnDestroyOrb;
                @DestroyOrb.canceled -= m_Wrapper.m_PlayerMovementActionsCallbackInterface.OnDestroyOrb;
                @ThrowPlayerOrb.started -= m_Wrapper.m_PlayerMovementActionsCallbackInterface.OnThrowPlayerOrb;
                @ThrowPlayerOrb.performed -= m_Wrapper.m_PlayerMovementActionsCallbackInterface.OnThrowPlayerOrb;
                @ThrowPlayerOrb.canceled -= m_Wrapper.m_PlayerMovementActionsCallbackInterface.OnThrowPlayerOrb;
                @Reset.started -= m_Wrapper.m_PlayerMovementActionsCallbackInterface.OnReset;
                @Reset.performed -= m_Wrapper.m_PlayerMovementActionsCallbackInterface.OnReset;
                @Reset.canceled -= m_Wrapper.m_PlayerMovementActionsCallbackInterface.OnReset;
                @ThrowDirectionKeyboardMouse.started -= m_Wrapper.m_PlayerMovementActionsCallbackInterface.OnThrowDirectionKeyboardMouse;
                @ThrowDirectionKeyboardMouse.performed -= m_Wrapper.m_PlayerMovementActionsCallbackInterface.OnThrowDirectionKeyboardMouse;
                @ThrowDirectionKeyboardMouse.canceled -= m_Wrapper.m_PlayerMovementActionsCallbackInterface.OnThrowDirectionKeyboardMouse;
            }
            m_Wrapper.m_PlayerMovementActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Walk.started += instance.OnWalk;
                @Walk.performed += instance.OnWalk;
                @Walk.canceled += instance.OnWalk;
                @Jump.started += instance.OnJump;
                @Jump.performed += instance.OnJump;
                @Jump.canceled += instance.OnJump;
                @ThrowEnvironmentOrb.started += instance.OnThrowEnvironmentOrb;
                @ThrowEnvironmentOrb.performed += instance.OnThrowEnvironmentOrb;
                @ThrowEnvironmentOrb.canceled += instance.OnThrowEnvironmentOrb;
                @ThrowDirectionController.started += instance.OnThrowDirectionController;
                @ThrowDirectionController.performed += instance.OnThrowDirectionController;
                @ThrowDirectionController.canceled += instance.OnThrowDirectionController;
                @DestroyOrb.started += instance.OnDestroyOrb;
                @DestroyOrb.performed += instance.OnDestroyOrb;
                @DestroyOrb.canceled += instance.OnDestroyOrb;
                @ThrowPlayerOrb.started += instance.OnThrowPlayerOrb;
                @ThrowPlayerOrb.performed += instance.OnThrowPlayerOrb;
                @ThrowPlayerOrb.canceled += instance.OnThrowPlayerOrb;
                @Reset.started += instance.OnReset;
                @Reset.performed += instance.OnReset;
                @Reset.canceled += instance.OnReset;
                @ThrowDirectionKeyboardMouse.started += instance.OnThrowDirectionKeyboardMouse;
                @ThrowDirectionKeyboardMouse.performed += instance.OnThrowDirectionKeyboardMouse;
                @ThrowDirectionKeyboardMouse.canceled += instance.OnThrowDirectionKeyboardMouse;
            }
        }
    }
    public PlayerMovementActions @PlayerMovement => new PlayerMovementActions(this);
    private int m_ControllerSchemeIndex = -1;
    public InputControlScheme ControllerScheme
    {
        get
        {
            if (m_ControllerSchemeIndex == -1) m_ControllerSchemeIndex = asset.FindControlSchemeIndex("Controller");
            return asset.controlSchemes[m_ControllerSchemeIndex];
        }
    }
    private int m_KeyboardMouseSchemeIndex = -1;
    public InputControlScheme KeyboardMouseScheme
    {
        get
        {
            if (m_KeyboardMouseSchemeIndex == -1) m_KeyboardMouseSchemeIndex = asset.FindControlSchemeIndex("KeyboardMouse");
            return asset.controlSchemes[m_KeyboardMouseSchemeIndex];
        }
    }
    public interface IPlayerMovementActions
    {
        void OnWalk(InputAction.CallbackContext context);
        void OnJump(InputAction.CallbackContext context);
        void OnThrowEnvironmentOrb(InputAction.CallbackContext context);
        void OnThrowDirectionController(InputAction.CallbackContext context);
        void OnDestroyOrb(InputAction.CallbackContext context);
        void OnThrowPlayerOrb(InputAction.CallbackContext context);
        void OnReset(InputAction.CallbackContext context);
        void OnThrowDirectionKeyboardMouse(InputAction.CallbackContext context);
    }
}
