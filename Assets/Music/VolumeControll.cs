﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class VolumeControll : MonoBehaviour
{
    public AudioMixer Mixer;

    public void SetMasterVolume(float vol)
    {
        Mixer.SetFloat("MasterVolume", Mathf.Log10(vol) * 20);
    }
    
    public void SetMusicVolume(float vol)
    {
        Mixer.SetFloat("MusicVolume", Mathf.Log10(vol) * 20);
    }
    
    public void SetSFXVolume(float vol)
    {
        Mixer.SetFloat("SFXVolume", Mathf.Log10(vol) * 20);
    }
    
}
